SRCS := $(wildcard markdown/*.md)
HTML := $(SRCS:markdown/%.md=%.html)
vpath %.md markdown
vpath %.html html

%.html: %.md
	pandoc --no-highlight -t revealjs --template templates/revealjs.pandoc -o html/$@ $<

default: ornek-sunum.html

build: $(HTML) index.md
	cp -R reveal.js styles html
	pandoc --standalone -o html/index.html index.md

clean:
	rm -rf html/*.html html/images html/reveal.js html/styles


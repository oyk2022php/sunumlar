# Mustafa Akgül Özgür Yazılım Yaz Kampı 2022 PHP Sunumları

Kamp içerisinde işlenmiş konularda "çoğunlukla" teknik başlıklar için oluşturulmuş sunumları kapsar.

## Sunumlara Nasıl Erişebilirim?

[https://oyk2022php.gitlab.io/sunumlar](https://oyk2022php.gitlab.io/sunumlar) adresinden sunumlara erişebilirsiniz.

## Nasıl Destek Olabilirim?

- Kurs içerisinde işlenmiş bir konuyla ilgili olarak yararlı olabileceğini düşündüğünüz başlıklarda sunum oluşturarak,
- Oluşturulmuş sunumlardaki hataları düzelterek veya ek bilgiler ekleyerek destek olabilirsiniz :)

Herhangi bir sunumun kaynağını `markdown` dizini içerisinde bulabilirsiniz. Markdown nedir diyorsanız hemen [inceleyin](https://www.markdownguide.org) :)

Göndermeden önce yerelinizde denemek isterseniz, sisteminizde şu araçların kurulu olması gerekiyor:

- [pandoc](https://pandoc.org/)
- [make](https://www.gnu.org/software/make/) (opsiyonel)

`make build` ile yerelinizde oluşturabilirsiniz veya `make clean` ile önceki oluşturulan ortamı silebilirsiniz.

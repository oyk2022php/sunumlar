---
author: Yazan Kişi
title: Sunum Başlığı
date: Sunum Tarihi
revealjs-url: 'reveal.js'
theme: night
css:
  - styles/agate.css
---

## 1. Slayt

# 2. Slayt (Alt Slaytları Var)

## 2.1. Slayt

- Her slayt `<h2>` elemanı ile başlar (markdown karşılığı `"##"`).
- `<h1>` elemanı kullanarak (markdown karşılığı `"#"`) slaytları gruplayabilirsiniz.

## 2.2. Slayt

Sunum içerisine kod da yerleştirebilirsiniz.

```php
// public/index.php

require __DIR__.'/../vendor/autoload.php';
```

